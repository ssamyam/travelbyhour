import React, { Component } from 'react';
import logo from './logo.svg';
import { BrowserRouter } from 'react-router-dom'
import NavBar from './components/Navbar/Navbar'
import './App.css';
import './components/Navbar/Navbar';
import {
  Route,
  NavLink,
  HashRouter
} from "react-router-dom";
import Signin from "./components/Signin/Signin";
import Rider from "./components/Rider/Rider";
import Driver from "./components/Driver/Driver";


const Main = (props) => {
  return (
      <div style={{marginTop:40}}>hiii</div>
  );
};

class App extends Component {
  render() {
    return (
      <div>  
        <div>
        <HashRouter>
            <div>
               <NavBar></NavBar>

                <div className="content">
                  
                  <Route 
                    path="/Signup" 
                    render={(props) => <Signin {...props}  />}
                    />
                  <Route 
                    path="/Driver" 
                    render={(props) => <Driver {...props}  />}
                    />
                  <Route 
                    path="/Rider" 
                    render={(props) => <Rider {...props}  />}
                    />
                </div>
                
            </div>
            
        </HashRouter>
        </div>
        
      </div>
    );
  }
}

export default App;
