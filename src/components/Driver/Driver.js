import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Faq from './Faq';
import Profile from './Profile';
import Referral from './Referral';
import TourHistory from './TourHistory';
import Earnings from './Earnings';

const styles = theme => ({

    root: {
        flexGrow: 1,
        paddingTop: 20
    },
    paper: {
        padding: theme.spacing.unit * 2,
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    background: {
        backgroundColor: '#E0F2F1',
        paddingTop: 20,
        paddingBottom: 120,
        // display: 'none'
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap',
        padding: 20
    },
    button: {
        height: 10,
        marginTop: 10
    }
});

class Driver extends React.Component {

    constructor(props) {
        super(props);

    }

    state = {
        value: 0,
    };

    handleChange = (event, value) => {
        this.setState({ value });
    };

    render() {
        const { classes } = this.props;
        const { value } = this.state;

        document.body.classList.add('innerPage');
        return (
            <Paper square>
                <Tabs
                    value={this.state.value}
                    indicatorColor="primary"
                    textColor="primary"
                    onChange={this.handleChange}
                    centered
                >
                    <Tab label="Profile" />
                    <Tab label="Earnings" />
                    <Tab label="Tour History" />
                    <Tab label="FAQ" />
                </Tabs>
                {value === 0 && <Profile classes={classes}>Item One</Profile>}
                {value === 1 && <Earnings classes={classes}>Item Two</Earnings>}
                {value === 2 && <TourHistory classes={classes}>Item Three</TourHistory>}
                {value === 3 && <Faq classes={classes}>Item Three</Faq>}
            </Paper>
        );
    }
}

// export default Driver;
export default withStyles(styles)(Driver);
