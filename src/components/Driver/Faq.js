import React from 'react';

import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: 30,
        textAlign: 'center',
        color: 'red',
    },
    pageTitle: {
        paddingTop: 10,
        paddingBottom: 20
    }
});

const faq = (props) => {
    const { classes } = props;
   return  (
    
    <div className={classes.background}>
    <div className="container">
        <h2 className={classes.pageTitle}>FAQ</h2>
        <Paper className={classes.root}>
            <Grid container spacing={24} style={{paddingLeft:30}}>
                <span>

TravelByHour (FAQ)

Triver

Q1. How to get Triver to sign up?

We will sign our friends and families who are existing ride sharing drivers. These drivers will be become our brand ambassador of our products. We need to use social media like facebook, linkedin, instagram, google etc. We will have an incentive program for them. 


Q2. How to attracts UBER and Lyft drivers?

Use UBER pain points against it. Here are the current pain points of UBER’s driver

Lack of training. They have to deal with numbers of unknown unwanted surprises. For example, how to handle drunk passenger, who still can rate driver at the end of the ride. Bad rating will hurt drivers income. UBER has 5 minutes training videos. 
Drivers not able to reach to UBER corporate employee.
They are relying on train an error. 
Lyft provides mentors to the new drivers. They follow the mentor during their training period and then only they will be running on their own. This model is working better between UBER and Lyft.

                </span>
            </Grid>
            </Paper>
            </div>
            </div>
   )

   
};

// export default faq
export default withStyles(styles)(faq);




