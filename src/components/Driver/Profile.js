import React from 'react';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { withStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';


const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: 30,
        textAlign: 'center',
        color: 'red',
    },
    pageTitle: {
        paddingTop: 10,
        paddingBottom: 20
    }
});

const profile = (props) => {

    const { classes } = props;
    return (
        <div className={classes.background}>
            <div className="container">

                <h2 className={classes.pageTitle}>Driver's Profile</h2>

                <Paper style={{ padding: 20 }}>
                    <Paper style={{ margin: 30, padding: 30 }}>
                        <h3> Login Information</h3>
                        {/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  Login Information @@@@@@@@@ */}
                        {/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  Login Information @@@@@@@@@ */}
                        <Grid container spacing={24}>

                            <Grid item xs={12} sm={6}>

                                <TextField
                                    id="standard-with-placeholder"
                                    label="User Name"
                                    placeholder="Please enter a valid email address"
                                    className={props.classes.textField}
                                    margin="normal"
                                    fullWidth
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>


                            </Grid>
                            <Grid item xs={12} sm={6}>

                                <TextField
                                    id="standard-with-placeholder"
                                    label="Password"
                                    placeholder="Password"
                                    className={props.classes.textField}
                                    margin="normal"
                                    fullWidth
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>


                            </Grid>
                            <Grid item xs={12} sm={6}>

                                <TextField
                                    id="standard-with-placeholder"
                                    label="Password Confirmation"
                                    placeholder="Password Confirmation"
                                    className={props.classes.textField}
                                    margin="normal"
                                    fullWidth
                                />
                            </Grid>

                        </Grid>
                    </Paper>
                    <Paper style={{ margin: 30, padding: 30 }}>
                        <h3> Driver Information</h3>
                        {/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  Login Information @@@@@@@@@ */}
                        {/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  Login Information @@@@@@@@@ */}
                        <Grid container spacing={24}>

                            <Grid item xs={12} sm={6}>

                                <TextField
                                    id="standard-with-placeholder"
                                    label="First Name"
                                    placeholder="First Name"
                                    className={props.classes.textField}
                                    margin="normal"
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>

                                <TextField
                                    id="standard-with-placeholder"
                                    label="Middle Name"
                                    placeholder="Middle Name"
                                    className={props.classes.textField}
                                    margin="normal"
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>

                                <TextField
                                    id="standard-with-placeholder"
                                    label="Last Name"
                                    placeholder="Last Name"
                                    className={props.classes.textField}
                                    margin="normal"
                                />
                            </Grid>

                            <Grid item xs={12} sm={6}>

                                <TextField
                                    id="standard-with-placeholder"
                                    label="Driver Liscense"
                                    placeholder="Driver Liscense"
                                    className={props.classes.textField}
                                    margin="normal"
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>

                                <TextField
                                    id="standard-with-placeholder"
                                    label="Street Address"
                                    placeholder="Street Address"
                                    className={props.classes.textField}
                                    margin="normal"
                                    fullWidth
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>

                                <TextField
                                    id="standard-with-placeholder"
                                    label="City"
                                    placeholder="City"
                                    className={props.classes.textField}
                                    margin="normal"
                                    fullWidth
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>

                                <TextField
                                    id="standard-with-placeholder"
                                    label="Primary Phone Number"
                                    placeholder="Primary Phone Number"
                                    className={props.classes.textField}
                                    margin="normal"
                                    fullWidth
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>

                                <TextField
                                    id="standard-with-placeholder"
                                    label="Secondary Phone Number"
                                    placeholder="Secondary Phone Number"
                                    className={props.classes.textField}
                                    margin="normal"
                                    fullWidth
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>

                                <TextField
                                    id="standard-with-placeholder"
                                    label="Emergency Contact Person"
                                    placeholder="Emergency Contact Person"
                                    className={props.classes.textField}
                                    margin="normal"
                                    fullWidth
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>

                                <TextField
                                    id="standard-with-placeholder"
                                    label="Emergency Contact Person's Number"
                                    placeholder="Emergency Contact Person's Number"
                                    className={props.classes.textField}
                                    margin="normal"
                                    fullWidth

                                />
                            </Grid>

                        </Grid>
                    </Paper>
                    <Paper style={{ margin: 30, padding: 30 }}>
                        <h3> Vehicle Information</h3>
                        {/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  Vehicle Information @@@@@@@@@ */}
                        {/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  Vehicle Information @@@@@@@@@ */}
                        <Grid container spacing={24}>
                            <Grid item xs={12} sm={6}>

                                <TextField
                                    id="standard-with-placeholder"
                                    label="License Plate Number"
                                    placeholder="License Plate Number"
                                    className={props.classes.textField}
                                    margin="normal"
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>

                                <TextField
                                    id="standard-with-placeholder"
                                    label="Vehicle Make"
                                    placeholder="Vehicle Make"
                                    className={props.classes.textField}
                                    margin="normal"
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>

                                <TextField
                                    id="standard-with-placeholder"
                                    label="Year Model"
                                    placeholder="Year Model"
                                    className={props.classes.textField}
                                    margin="normal"
                                />
                            </Grid>

                            <Grid item xs={12} sm={6}>

                                <TextField
                                    id="standard-with-placeholder"
                                    label="Model or Series"
                                    placeholder="Model or Series"
                                    className={props.classes.textField}
                                    margin="normal"
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>

                                <TextField
                                    id="standard-with-placeholder"
                                    label="Body Type Model"
                                    placeholder="Body Type Model"
                                    className={props.classes.textField}
                                    margin="normal"
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>

                                <TextField
                                    id="standard-with-placeholder"
                                    label="Fuel Type"
                                    placeholder="Fuel Type"
                                    className={props.classes.textField}
                                    margin="normal"
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>

                                <TextField
                                    id="standard-with-placeholder"
                                    label="Number of Seats"
                                    placeholder="Number of Seats"
                                    className={props.classes.textField}
                                    margin="normal"
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>

                                <TextField
                                    id="standard-with-placeholder"
                                    label="Color"
                                    placeholder="Color"
                                    className={props.classes.textField}
                                    margin="normal"
                                />
                            </Grid>

                            <Grid item xs={12} sm={6}>

                                <TextField
                                    id="standard-with-placeholder"
                                    label="Vin Number"
                                    placeholder="Vin Number"
                                    className={props.classes.textField}
                                    margin="normal"
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>

                                <TextField
                                    id="standard-with-placeholder"
                                    label="State of Registration"
                                    placeholder="State of Registration"
                                    className={props.classes.textField}
                                    margin="normal"
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>

                                <label htmlFor="contained-button-file">
                                    <Button variant="contained" component="span" className={classes.button}>
                                        Upload Car Image
                                    </Button>
                                </label>
                            </Grid>
                        </Grid>
                    </Paper>

                    <Paper style={{ margin: 30, padding: 30 }}>
                        <h3> Bank Information</h3>
                        {/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  Vehicle Information @@@@@@@@@ */}
                        {/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  Vehicle Information @@@@@@@@@ */}
                        <Grid container spacing={24}>
                            <Grid item xs={12} sm={6}>

                                <TextField
                                    id="standard-with-placeholder"
                                    label="Bank Name"
                                    placeholder="Bank Name"
                                    className={props.classes.textField}
                                    margin="normal"
                                    fullWidth
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>
                            </Grid>
                            <Grid item xs={12} sm={6}>

                                <TextField
                                    id="standard-with-placeholder"
                                    label="Bank Account No"
                                    placeholder="Your bank Account No"
                                    className={props.classes.textField}
                                    margin="normal"
                                    fullWidth
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>
                            </Grid>
                            <Grid item xs={12} sm={6}>

                                <TextField
                                    id="standard-with-placeholder"
                                    label="Routing Number"
                                    placeholder="Routing Number"
                                    className={props.classes.textField}
                                    margin="normal"
                                    fullWidth
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>
                            </Grid>
                        </Grid>
                    </Paper>

                    <Grid container direction="row" spacing={24} justify="flex-end" style={{marginBottom: 40}}>
                        <label htmlFor="contained-button-file" style={{marginRight:40}}>
                            <Button size="large" variant="contained" component="span" className={classes.button}>
                                Cancel
                            </Button>
                        </label>
                        <label htmlFor="contained-button-file" style={{marginRight:40}}>
                            <Button color="primary" size="large" variant="contained" component="span" className={classes.button}>
                                Apply
                            </Button>
                        </label>
                    </Grid>
                </Paper>
            </div>
        </div>

    )


};

// export default profile
export default withStyles(styles)(profile);


