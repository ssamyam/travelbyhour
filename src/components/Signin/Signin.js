import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import IconButton from '@material-ui/core/IconButton';
import InputAdornment from '@material-ui/core/InputAdornment';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import './../../App.css';


const styles = theme => ({
    root: {
        flexGrow: 1,
        paddingTop: 20
    },
    paper: {
        padding: theme.spacing.unit * 2,
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    background: {
        backgroundColor: '#e6e6e6',
        paddingTop: 120,
        paddingBottom: 120,
        // display: 'none'
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap',
        padding: 20
    },
    button: {
        height: 10,
        marginTop: 10
    },
});

class Signin extends React.Component {

    constructor(props) {
        super(props);
    }

    state = {
        amount: '',
        passwordDriver: '',
        passwordRider: '',
        weight: '',
        weightRange: '',
        showPasswordDriver: false,
        showPasswordRider: false,
    };


    driverSignupHandler = (evt) => {
        // evt.preventDefault();
        this.props.history.push('Driver');
    }

    riderSignupHandler = (evt) => {
        // evt.preventDefault();
        this.props.history.push('Rider');
    }

    driverSignInHandler = (evt) => {
        debugger;
    }


    riderSignInHandler = (evt) => {
        debugger;
    }

    handleClickShowPasswordRider = () => {
        this.setState(state => ({ showPasswordRider: !state.showPasswordRider }));
    };
    handleClickShowPasswordDriver = () => {
        this.setState(state => ({ showPasswordDriver: !state.showPasswordDriver }));
    };

    handleChangeDriver = prop => event => {
        this.setState({ [prop]: event.target.value });
    };


    handleChangeRider = prop => event => {
        this.setState({ [prop]: event.target.value });
    };



    render() {
        const { classes } = this.props;
        return (

            <section className={classes.background + " mbr-section mbr-section__container article header3-c"} id="SignupLinkMain">
                <div className="container">
                    <div className="row">
                        <div className="col-xs-12">
                            <h3 className="mbr-section-title">Sign in</h3>
                            <div className={classes.root}>
                                <Grid container spacing={24}>

                                    <Grid item xs={12} sm={6}>
                                        <h4>
                                            <a>Driver</a>
                                        </h4>
                                        <Card className={classes.card}>
                                            <CardContent>
                                                <form className={classes.container} noValidate autoComplete="off">
                                                    <TextField
                                                        id="outlined-name"
                                                        label="User Name"
                                                        margin="normal"
                                                        variant="outlined"
                                                        fullWidth
                                                    />
                                                    <TextField
                                                        id="outlined-name"
                                                        label="Password"
                                                        margin="normal"
                                                        variant="outlined"
                                                        type={this.state.showPasswordDriver ? 'text' : 'password'}
                                                        value={this.state.passwordDriver}
                                                        onChange={this.handleChangeDriver('passwordDriver')}
                                                        fullWidth
                                                        InputProps={{
                                                            endAdornment: (
                                                                <InputAdornment position="end">
                                                                    <IconButton
                                                                        aria-label="Toggle password visibility"
                                                                        onClick={this.handleClickShowPasswordDriver}
                                                                    >
                                                                        {this.state.showPasswordDriver ? <VisibilityOff /> : <Visibility />}
                                                                    </IconButton>
                                                                </InputAdornment>
                                                            ),
                                                        }}
                                                    />
                                                    <Button
                                                        size="medium"
                                                        variant="contained"
                                                        color="primary"
                                                        className={classes.button}>
                                                        Sign in
                                            </Button>
                                                </form>

                                                <span>Don't have an account? <a onClick={this.driverSignupHandler}> Signup</a></span>
                                            </CardContent>
                                        </Card>
                                    </Grid>
                                    <Grid item xs={12} sm={6}>
                                        <h4>
                                            <a>Rider</a>
                                        </h4>
                                        <Card className={classes.card}>
                                            <CardContent>
                                                <form className={classes.container} noValidate autoComplete="off">
                                                    <TextField
                                                        id="outlined-name"
                                                        label="User Name"
                                                        margin="normal"
                                                        variant="outlined"
                                                        fullWidth
                                                    />
                                                    <TextField
                                                        id="outlined-name"
                                                        label="Password"
                                                        margin="normal"
                                                        variant="outlined"
                                                        type={this.state.showPasswordRider ? 'text' : 'password'}
                                                        value={this.state.passwordRider}
                                                        onChange={this.handleChangeRider('passwordRider')}
                                                        fullWidth
                                                        InputProps={{
                                                            endAdornment: (
                                                                <InputAdornment position="end">
                                                                    <IconButton
                                                                        aria-label="Toggle password visibility"
                                                                        onClick={this.handleClickShowPasswordRider}
                                                                    >
                                                                        {this.state.showPasswordRider ? <VisibilityOff /> : <Visibility />}
                                                                    </IconButton>
                                                                </InputAdornment>
                                                            ),
                                                        }}
                                                    />
                                                    <Button size="medium" variant="contained" color="primary" className={classes.button}>
                                                        Sign in
                                            </Button>
                                                </form>
                                                <span>Don't have an account? <a onClick={this.riderSignupHandler}> Signup</a></span>
                                            </CardContent>
                                        </Card>
                                    </Grid>
                                </Grid>
                            </div>
                            <small className="mbr-section-subtitle"></small>
                        </div>
                    </div>
                </div>
            </section>



        );
    }
}

Signin.propTypes = {
    classes: PropTypes.object.isRequired,
};

// export default Signin;
export default withStyles(styles)(Signin);
