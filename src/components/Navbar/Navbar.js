import React from 'react';
import Button from '@material-ui/core/Button';
import './../../App.css';
import { withRouter } from 'react-router-dom';
class NavBar extends React.Component {

    constructor(props) {
        super(props);
        this.showLogin = this.showLogin.bind(this);
    
    }
    showLogin = (evt) => {
        let elem;
        let attr;
        let evts = evt;

        evt.preventDefault();
        
        if(evt && evt.target.innerText === 'Signup'){
            this.props.history.push('/Signup');
            // this.props.histoy.push('/Signup');
            document.getElementById('mainContainer').classList.add('signIn');
        }
        else{

            document.getElementById('mainContainer').classList.remove('signIn');
            // attr = evts.target.getAttribute('dataid');
            // elem = document.getElementById(attr);
            // setTimeout( () => {
            //     debugger;
            //     elem.scrollIntoView();
            // }, 0, elem)
       }
    //    evt.preventDefault();
      
   }


    render() {
        
        return (
            

        <section className="menu-0" id="menu-0">
            <nav className="navbar navbar-dropdown bg-color transparent navbar-fixed-top">
                <div className="container">

                    <div className="mbr-table">
                        <div className="mbr-table-cell">
                            <div className="navbar-brand">
                                <a className="navbar-caption" href="#">Triver</a>
                               
                            </div>

                        </div>
                        <div className="mbr-table-cell">

                            <button className="navbar-toggler pull-xs-right hidden-md-up" type="button" data-toggle="collapse"
                                data-target="#exCollapsingNavbar">
                                <div className="hamburger-icon"></div>
                            </button>

                            <ul 
                                onClick={this.showLogin}
                                className="nav-dropdown collapse pull-xs-right nav navbar-nav navbar-toggleable-sm" id="exCollapsingNavbar">
                                <li className="nav-item dropdown">
                                    <a className="nav-link link" href="index.html#msg-box8-1">About
                                        Triver</a></li>
                                <li className="nav-item">
                                    <a className="nav-link link" dataid="DriverLinkMain" href="index.html#DriverLinkMain">Driver</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link link" dataid="RiderLinkMain" href="index.html#RiderLinkMain">Rider</a>
                                </li>
                                <li className="nav-item" id="SignupLink" >
                                    <a className="nav-link link" dataid="RiderLinkMain">Signup</a>
                                </li>
                            </ul>

                            <button className="navbar-toggler navbar-close collapsed" type="button" data-toggle="collapse"
                                data-target="#exCollapsingNavbar">
                                <div className="close-icon"></div>
                            </button>

                        </div>
                    </div>

                </div>
            </nav>

        </section>
        );
    }
}
export default withRouter(NavBar);
// export default NavBar;